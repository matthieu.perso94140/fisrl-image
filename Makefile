VERSION=python

build:
	docker build -t fisrl-image:$(VERSION) .

test-dockerfile:
	docker run --rm -i hadolint/hadolint < Dockerfile

test-image:
	docker run --rm -i fisrl-image:$(VERSION) < test.sh
