FROM python:3.8.5-slim-buster

RUN useradd -m docker --uid=1000
RUN pip install --no-cache-dir gym==0.17.1 matplotlib==3.3.2 numpy==1.19.2 tqdm==4.45.0 PyYAML==5.3.1
WORKDIR /home/docker/workspace
USER 1000:1000

CMD ["/bin/sh"]
